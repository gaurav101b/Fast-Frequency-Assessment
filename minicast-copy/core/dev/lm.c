/*
 * Copyright (c) 2011, ETH Zurich.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Federico Ferrari <ferrari@tik.ee.ethz.ch>
 *
 */

/**
 * \file
 *         Glossy core, source file.
 * \author
 *         Federico Ferrari <ferrari@tik.ee.ethz.ch>
 */

#include "lm.h"
#include "node-id.h"
#include "glossy.h"


#define CM_POS              CM_1
#define CM_NEG              CM_2
#define CM_BOTH             CM_3


uint8_t flag_transmission_started;
uint8_t flag_transmission_ended;
uint8_t flag_reception_started;
uint8_t flag_reception_ended;

volatile uint8_t flag_radio_event;
volatile uint8_t flag_timer_event;


static volatile uint8_t state_measurement;

static uint8_t relay_counts_received[20];
static uint8_t num_relay_count_received;

static uint8_t transmit_or_receive;
static uint8_t packet_length;
static uint8_t init_id;
static int frequency;

static uint8_t initiator, sync, rx_cnt, tx_cnt, tx_max;
static uint8_t *data, *packet, *other_packet;
static uint8_t data_len, packet_len, packet_len_tmp, header,init_id_temp;
static uint8_t bytes_read, tx_relay_cnt_last, n_timeouts;
static volatile uint8_t state_glossy;

static rtimer_clock_t t_rx_start, t_rx_stop, t_tx_start, t_tx_stop, t_start;
static rtimer_clock_t t_rx_timeout;
static rtimer_clock_t T_irq;
static rtimer_clock_t t_stop;
static rtimer_clock_t t_stop_tdma;
static rtimer_callback_t cb;
static rtimer_callback_t cb_tdma;
static struct rtimer *rtimer;
static void *ptr;
static unsigned short ie1, ie2, p1ie, p2ie, tbiv;

static rtimer_clock_t T_slot_h, T_rx_h, T_w_rt_h, T_tx_h, T_w_tr_h, t_ref_l, T_offset_h, t_first_rx_l;
#if GLOSSY_SYNC_WINDOW
static unsigned long T_slot_h_sum;
static uint8_t win_cnt;
#endif /* GLOSSY_SYNC_WINDOW */
static uint8_t relay_cnt, t_ref_l_updated;



static inline void initialize_packets_buffers(){

	uint8_t i;
	for(i=0;i<128;i++)
		packet[i]=0;
}

/*------------------------------------------------------------------------------------------------*/
// TDMA reading functions -

inline uint8_t read_fifo_bytes(uint8_t *bytes, int num){

	uint8_t bytes_read=0;
	t_rx_timeout = t_rx_start + ((uint16_t)128* 35 + 200) * 4;	// For DCO
	while (bytes_read <num) {
		while (!FIFO_IS_1) {
			if (!RTIMER_CLOCK_LT(TBR, t_rx_timeout)) {
				radio_flush_rx();
				return 0;
			}
		}
		FASTSPI_READ_FIFO_BYTE(bytes[bytes_read++]);
	}
	return 1;

}



inline uint8_t read_byte(uint8_t *ptr, uint8_t check_errors, uint8_t min, uint8_t max){

	// First wait for the byte to come
	while (!FIFO_IS_1) {
		if (!RTIMER_CLOCK_LT(TBR,t_rx_timeout)) {
			radio_flush_rx();
			return 0;
		}
	};
	FASTSPI_READ_FIFO_BYTE(*ptr);
	if (check_errors &&  !((*ptr < min) && (*ptr > max))){
		radio_flush_rx();
		return 0;
	}
	bytes_read++;

	return 1;
}

//inline uint8_t read_header(uint8_t check_errors){
//
//	bytes_read=0;
//	t_rx_timeout = t_rx_start + ((uint16_t)128* 35 + 200) * 4;	// For DCO
//
//	/*----------------------------------------------------------------------------------------------*/
//	// 1. THE LENGTH FIELD
//	/*----------------------------------------------------------------------------------------------*/
//	if(!read_byte(&TDMA_OTHER_HEADER_LEN,1,2,127))
//		return 0;
//
//	if(!read_byte(&TDMA_OTHER_HEADER_TYPE,0,0,0))
//		return 0;
//
//	if(!read_byte(&TDMA_OTHER_HEADER_SENDER_ADDRESS,0,1,140))
//		return 0;
//
//	if(!read_byte(&TDMA_OTHER_HEADER_TENTATIVE_SCHEDULE_NUMBER,0,1,5))
//		return 0;
//
//	if(!read_byte(&TDMA_OTHER_HEADER_PACKET_TYPE,0,0,0))
//		return 0;
//
//	if(!read_byte(&TDMA_OTHER_HEADER_REST_NUM_BYTE,0,1,88))
//		return 0;
//
//	return 1;
//}


inline uint8_t read_header(uint8_t check_errors){

	bytes_read=0;
	t_rx_timeout = t_rx_start + ((uint16_t)128* 35 + 200) * 4;	// For DCO

	/*----------------------------------------------------------------------------------------------*/
	// 1. THE LENGTH FIELD
	/*----------------------------------------------------------------------------------------------*/
	while(!FIFO_IS_1)
	{
		if (!RTIMER_CLOCK_LT(TBR,t_rx_timeout)) {
			radio_flush_rx();
			return 0;
		}
	}
	FASTSPI_READ_FIFO_BYTE(packet[0]);
	if ((packet[0] < 2) || (packet[0] > 127)) {
		radio_flush_rx();
		return 0;
	}
	bytes_read++;
	/*----------------------------------------------------------------------------------------------*/
	// 2. THE HEADER FIELD - protocol type
	/*----------------------------------------------------------------------------------------------*/
	while (!FIFO_IS_1) {
		if (!RTIMER_CLOCK_LT(TBR,t_rx_timeout)) {
			radio_flush_rx();
			return 0;
		}
	};
	FASTSPI_READ_FIFO_BYTE(packet[1]);

	//	if(((packet[bytes_read] | TDMA_HEADER)  & ~TDMA_HEADER_MASK)!=r_header){
	if (check_errors && (packet[1]=packet[1] || packet[1] > 15) ){
		radio_flush_rx();
		return 0;
	}
	bytes_read++;
	/*----------------------------------------------------------------------------------------------*/
	//
	/*----------------------------------------------------------------------------------------------*/
	// 3. THE ADDRESS FIELD - the sender's address
	/*----------------------------------------------------------------------------------------------*/

	while (!FIFO_IS_1) {
		if (!RTIMER_CLOCK_LT(TBR,t_rx_timeout)) {
			radio_flush_rx();
			return 0;
		}
	};
	FASTSPI_READ_FIFO_BYTE(packet[2]);
	if (check_errors && !((packet[2] > 0) && (packet[2] < 140))) {
		//printf("3xx %u n",TDMA_OTHER_HEADER_SENDER_ADDRESS);
		radio_flush_rx();
		return 0;
	}
	bytes_read++;
	//----------------------------------------------------------------------------------------------

	//----------------------------------------------------------------------------------------------
	// 4. THE ADDRESS FIELD - the intended receivers's address
	//----------------------------------------------------------------------------------------------
	while (!FIFO_IS_1) {
		if (!RTIMER_CLOCK_LT(TBR,t_rx_timeout)) {
			radio_flush_rx();
			return 0;
		}
	};
	FASTSPI_READ_FIFO_BYTE(packet[3]);
	if (check_errors &&  !((packet[3] >= 1) && (packet[3] <= 5))) {
		radio_flush_rx();
		return 0;
	}
	bytes_read++;
	//----------------------------------------------------------------------------------------------

	//----------------------------------------------------------------------------------------------
	// 5. THE PACKET TYPE - the type of the packet
	//----------------------------------------------------------------------------------------------
	while (!FIFO_IS_1) {
		if (!RTIMER_CLOCK_LT(TBR,t_rx_timeout)) {
			radio_flush_rx();
			return 0;
		}
	};
	FASTSPI_READ_FIFO_BYTE(packet[4]);
	if (check_errors &&  !(((uint8_t)(packet[4])>= packet[4])
			&& ((uint8_t)(packet[4]) <=packet[4]))) {
		radio_flush_rx();
		return 0;
	}
	bytes_read++;

	//----------------------------------------------------------------------------------------------
	// 6. THE REST MEANINGFUL BYTES - the type of the packet
	//----------------------------------------------------------------------------------------------
	while (!FIFO_IS_1) {
		if (!RTIMER_CLOCK_LT(TBR,t_rx_timeout)) {
			radio_flush_rx();
			return 0;
		}
	};
	FASTSPI_READ_FIFO_BYTE(packet[5]);
	// Check this 6th byte only for tentative schedule and final schedule
	if (check_errors &&  (packet[5]<1 || packet[5]>92)) {
		radio_flush_rx();
		return 0;
	}
	/*----------------------------------------------------------------------------------------------*/
	return 1;
}

/*------------------------------------------------------------------------------------------------*/
inline void state_machine_measurement(unsigned short tbiv){

	volatile uint8_t i = 0;

	if(state_radio == STATE_RADIO_TRANSMITTING && SFD_IS_1){

		state_radio = STATE_RADIO_TRANSMITTING_NORMAL;
		flag_transmission_started = 1;

	} else {

		if(state_radio == STATE_RADIO_WAITING && SFD_IS_1){
			// Reception started
			t_rx_start = TBCCR1;
			state_radio = STATE_RADIO_RECEIVING_NORMAL;
//			read_header(0);
//			radio_flush_rx();
			read_fifo_bytes(packet, packet_length);

			//state_radio = STATE_RADIO_WAITING;
			flag_reception_started = 1;
		}else {
			if(state_radio == STATE_RADIO_RECEIVING_NORMAL && !SFD_IS_1){
				// Reception finished
				state_radio = STATE_RADIO_WAITING;
				radio_flush_rx();
				flag_reception_ended = 1;
			}else {

				if(state_radio == STATE_RADIO_TRANSMITTING_NORMAL && !SFD_IS_1){
					// Transmission ended
					state_radio = STATE_RADIO_WAITING;
					ENERGEST_OFF(ENERGEST_TYPE_TRANSMIT);
					ENERGEST_ON(ENERGEST_TYPE_LISTEN);
					radio_flush_tx();
					state_radio = STATE_RADIO_WAITING;
					flag_transmission_ended = 1;
				}
			}
		}
	}
}



/* --------------------------- Glossy process ----------------------- */
PROCESS(measurement_process, "TDMA busy-waiting process");
PROCESS_THREAD(measurement_process, ev, data) {
	PROCESS_BEGIN();

	do {
		packet = (uint8_t *) malloc(128);

	} while (packet == NULL);

	while (1) {

		PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_POLL);

		while (MEASUREMENT_IS_ON() && RTIMER_CLOCK_LT(RTIMER_NOW(), t_stop_tdma));
#if COOJA
		//while (state_measurement == STATE_MEASUREMENT_TRANSMITTING_PACKET);
#endif /* COOJA */
		// Glossy finished: execute the callback function
		dint();
		cb_tdma(rtimer, ptr);
		eint();
	}

	PROCESS_END();
}


void measurement_start (
		uint8_t transmit_or_receive_,
		uint8_t packet_length_,
		uint8_t init_id_,
		int frequency_,
		rtimer_callback_t cb_,
		struct rtimer *rt_,
		void *ptr_,
		rtimer_clock_t t_stop_)
{

	cb_tdma = cb_;
	rtimer = rt_;
	ptr = ptr_;
	t_stop_tdma = t_stop_;
	transmit_or_receive = transmit_or_receive_;
	packet_length = packet_length_;
	init_id=init_id_;
	frequency = frequency_;
	initialize_packets_buffers();
	glossy_disable_other_interrupts();


//    int s = splhigh();
//	TBCCTL0 = 0;
//	DISABLE_FIFOP_INT();
//	CLEAR_FIFOP_INT();
//	SFD_CAP_INIT(CM_BOTH);
//	ENABLE_SFD_INT();
//	// stop Timer B
//	TBCTL = 0;
//	// Timer B sourced by the DCO
//	TBCTL = TBSSEL1;
//	// start Timer B
//	TBCTL |= MC1;
//    splx(s);
//    watchdog_stop();

//	if(node_id==1){
	cc2420_set_frequency(frequency);

	if(transmit_or_receive==1){

		packet[0] = packet_length-1;
		packet[2] = init_id;
		packet_len_tmp = packet[0];

		volatile uint8_t gap;
		for(gap=0;gap<200;gap++);
		for(gap=0;gap<200;gap++);
//
		radio_flush_rx();
		radio_flush_tx();
		radio_write_tx(packet,packet_length);
		radio_start_tx();
		state_radio = STATE_RADIO_TRANSMITTING;
		state_measurement = STATE_MEASUREMENT_TRANSMITTING_PACKET;

	} else {

		state_radio = STATE_RADIO_WAITING;
		state_measurement = STATE_MEASUREMENT_WAITING_FOR_PACKET;
		radio_on();
	}

	process_poll(&measurement_process);
}

uint8_t measurement_stop(uint8_t *correctness, int *rssi, int *lqi, int *initiator_id,int frequency_to_sync){

	state_measurement = STATE_MEASUREMENT_OFF;

    radio_off();
	radio_flush_rx();
	radio_flush_tx();
    *correctness=0;
    *rssi=0;
    *lqi=0;
    *initiator_id=0;
	glossy_enable_other_interrupts();
	cc2420_set_frequency(frequency_to_sync);

//	int s = splhigh();
//	DISABLE_SFD_INT();
//	CLEAR_SFD_INT();
//	FIFOP_INT_INIT();
//	ENABLE_FIFOP_INT();
//	// stop Timer B
//	TBCTL = 0;
//	// Timer B sourced by the 32 kHz
//	TBCTL = TBSSEL0;
//	// start Timer B
//	TBCTL |= MC1;
//    splx(s);
//    watchdog_start();

	uint8_t ret=0;

	if(transmit_or_receive){
		if(flag_transmission_started==1 &&
			flag_transmission_ended==1){
			// Successfully transmitted
			ret=1;
		}
	} else {

		if(flag_reception_started==1 &&
			flag_reception_ended==1){
			// Successfully received
			*correctness = (uint8_t)packet[packet_length-1] & 0x80;
             if((uint8_t)packet[packet_length-1] & 0x80)
			{
     			*initiator_id=(uint8_t)(packet[2]);
     			*correctness=2;
			}
             else
             {
            	 *initiator_id=0;
            	 *correctness=1;
             }
		    signed char l_rssi = packet[packet_length-2];//flag_transmission_started;
		    *rssi = (int)(l_rssi)-45;
		    *lqi = packet[packet_length-1] & 0x7f;
		    ret=1;
		}
	}

    // Reset the flags
	flag_reception_started=0;
	flag_reception_ended=0;
	flag_transmission_started=0;
	flag_transmission_ended=0;

	return ret;

}


uint8_t get_measurement_state(void) {
	return state_measurement;
}

uint8_t get_pkt_length(void) {
	return init_id_temp;
}


