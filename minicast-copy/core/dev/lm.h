/*
 * Copyright (c) 2011, ETH Zurich.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Federico Ferrari <ferrari@tik.ee.ethz.ch>
 *
 */

/**
 * \file
 *         Glossy core, header file.
 * \author
 *         Federico Ferrari <ferrari@tik.ee.ethz.ch>
 */

#ifndef LM_H_
#define LM_H_

#include "contiki.h"
#include "dev/watchdog.h"
#include "dev/cc2420_const.h"
#include "dev/leds.h"
#include "dev/spi.h"
#include "lib/crc16.h"
#include <stdio.h>
#include <legacymsp430.h>
#include <stdlib.h>


volatile uint8_t controller;





#define NOP_DELAY() \
		do {\
			_NOP(); _NOP(); _NOP(); _NOP(); \
			_NOP(); _NOP(); _NOP(); _NOP(); \
			_NOP(); _NOP(); _NOP(); _NOP(); \
			_NOP(); _NOP(); _NOP(); _NOP(); \
			_NOP(); _NOP(); _NOP(); _NOP(); \
		} while (0);


#define SPI_SET_TXPOWER(c)\
		do {\
			/* Enable SPI */\
		/*SPI_ENABLE();\*/\
		/* Setting TXCTRL */\
		SPI_TXBUF = CC2420_TXCTRL;\
		NOP_DELAY();\
		SPI_TXBUF = ((u8_t) ((c) >> 8));\
		NOP_DELAY();\
		SPI_TXBUF = ((u8_t) (c));\
		NOP_DELAY();\
		/* Disable SPI */\
		/*SPI_DISABLE();\*/\
		} while (0)


#define MAX_POWER 31

/**
 * If not zero, nodes print additional debug information (disabled by default).
 */
#define GLOSSY_DEBUG 0
/**
 * Size of the window used to average estimations of slot lengths.
 */
#define GLOSSY_SYNC_WINDOW            64
/**
 * Initiator timeout, in number of Glossy slots.
 * When the timeout expires, if the initiator has not received any packet
 * after its first transmission it transmits again.
 */
#define GLOSSY_INITIATOR_TIMEOUT      3

/**
 * Ratio between the frequencies of the DCO and the low-frequency clocks
 */
#if COOJA
#define CLOCK_PHI                     (4194304uL / RTIMER_SECOND)
#else
#define CLOCK_PHI                     (F_CPU / RTIMER_SECOND)
#endif /* COOJA */




#define MEASUREMENT_IS_ON()                (get_measurement_state() != STATE_MEASUREMENT_OFF)

uint8_t initiator_id;
uint8_t hop_count;


uint8_t length_segment;
uint8_t length_req_packet;
uint8_t tentative_schedule_number;

uint8_t bytes_low_power_before;
uint8_t position;
uint8_t bytes_high;

uint8_t num_parents;
uint8_t num_children;
uint8_t num_siblings;
uint8_t num_ancestors;
uint8_t num_temp_nodes;

enum radio_event_flag_values {
	FLAG_RADIO_UNDEFINED,
	FLAG_PACKET_RECEPTION_STARTED,
	FLAG_PACKET_RECEPTION_ENDED,
	FLAG_PACKET_RECEPTION_ENDED_WITH_SYNCH_REQ,
	FLAG_PACKET_JUST_BEFORE_SYNCHRONOUS_TRANS,
	FLAG_PACKET_SYNCHRONOUS_TRANSMISSION_FAILED,
	FLAG_PACKET_RECEPTION_WAS_ABORTED,
	FLAG_PACKET_TRANSMISSION_STARTED,
	FLAG_PACKET_TRANSMISSION_TOWARDS_ENDING,
	FLAG_PACKET_TRANSMISSION_ENDED,
	FLAG_RADIO_MYSTERIOUS,
};

enum timer_event_flag_values {

	FLAG_TIMER_UNDEFINED,
	FLAG_TIMER_TBCCR2,
	FLAG_TIMER_TBCCR3,
	FLAG_TIMER_TBCCR4,
	FLAG_TIMER_TBCCR5,
	FLAG_TIMER_TBCCR6,
	FLAG_TIMER_MYSTERIOUS,
};



enum state_measurement_type {

	STATE_MEASUREMENT_OFF,
	STATE_MEASUREMENT_ON,
	STATE_MEASUREMENT_TRANSMITTING_PACKET,
	STATE_MEASUREMENT_WAITING_FOR_PACKET,
};

enum packet_type_values {
	PACKET_TYPE_PING,
	PACKET_TYPE_PONG,
};



#if GLOSSY_DEBUG
unsigned int high_T_irq, rx_timeout, bad_length, bad_header, bad_crc;
#endif /* GLOSSY_DEBUG */

PROCESS_NAME(measurement_process);
/* ----------------------- Application interface -------------------- */
/**
 * \defgroup glossy_interface Glossy API
 * @{
 * \file   glossy.h
 * \file   glossy.c
 */


void measurement_start(
		uint8_t measurement_initiator_id,
		uint8_t packet_length,
		uint8_t init_id_,
		int frequency_,
		rtimer_callback_t cb_,
		struct rtimer *rt_,
		void *ptr_,
		rtimer_clock_t t_stop_);
/**
 * \brief            Stop Glossy and resume all other application tasks.
 * \returns          Number of times the packet has been received during
 *                   last Glossy phase.
 *                   If it is zero, the packet was not successfully received.
 * \sa               get_rx_cnt
 */

//uint8_t measurement_stop(int *rssi, int *lqi);
uint8_t measurement_stop(uint8_t *correctness, int *f1, int *f2, int *f3,int frequency_to_sync);
/**
 * \brief            Get the last received counter.
 * \returns          Number of times the packet has been received during
 *                   last Glossy phase.
 *                   If it is zero, the packet was not successfully received.
 */
uint8_t get_rx_cnt(void);

uint8_t get_initiator_id(void);
uint8_t get_hop_count(void);

void print_relay_counts(void);
void print_list_of_children(void);
void print_list_of_parents(void);
void print_list_of_siblings(void);
void print_list_of_ancestors(void);
void print_dbg(void);
void print_packets();
/**
 * \brief            Get the current Glossy state.
 * \return           Current Glossy state, one of the possible values
 *                   of \link glossy_state \endlink.
 */
uint8_t get_state(void);

uint8_t get_measurement_state(void);
uint8_t get_pkt_length(void);


/**
 * \brief            Get low-frequency time of first packet reception
 *                   during the last Glossy phase.
 * \returns          Low-frequency time of first packet reception
 *                   during the last Glossy phase.
 */
rtimer_clock_t get_t_first_rx_l(void);

/** @} */

/**
 * \defgroup glossy_sync Interface related to time synchronization
 * @{
 */

/**
 * \brief            Get the last relay counter.
 * \returns          Value of the relay counter embedded in the first packet
 *                   received during the last Glossy phase.
 */
uint8_t get_relay_cnt(void);

/**
 * \brief            Get the local estimation of T_slot, in DCO clock ticks.
 * \returns          Local estimation of T_slot.
 */
rtimer_clock_t get_T_slot_h(void);

/**
 * \brief            Get low-frequency synchronization reference time.
 * \returns          Low-frequency reference time
 *                   (i.e., time at which the initiator started the flood).
 */
rtimer_clock_t get_t_ref_l(void);

/**
 * \brief            Provide information about current synchronization status.
 * \returns          Not zero if the synchronization reference time was
 *                   updated during the last Glossy phase, zero otherwise.
 */
uint8_t is_t_ref_l_updated(void);

/**
 * \brief            Set low-frequency synchronization reference time.
 * \param t          Updated reference time.
 *                   Useful to manually update the reference time if a
 *                   packet has not been received.
 */
void set_t_ref_l(rtimer_clock_t t);

/**
 * \brief            Set the current synchronization status.
 * \param updated    Not zero if a node has to be considered synchronized,
 *                   zero otherwise.
 */
void set_t_ref_l_updated(uint8_t updated);

/** @} */

/** @} */

/**
 * \defgroup glossy_internal Glossy internal functions
 * @{
 * \file   glossy.h
 * \file   glossy.c
 */


/**
 * \defgroup glossy_capture Timer capture of clock ticks
 * @{
 */

/* -------------------------- Clock Capture ------------------------- */
/**
 * \brief Capture next low-frequency clock tick and DCO clock value at that instant.
 * \param t_cap_h variable for storing value of DCO clock value
 * \param t_cap_l variable for storing value of low-frequency clock value
 */
#define CAPTURE_NEXT_CLOCK_TICK(t_cap_h, t_cap_l) do {\
		/* Enable capture mode for timers B6 and A2 (ACLK) */\
		TBCCTL6 = CCIS0 | CM_POS | CAP | SCS; \
		TACCTL2 = CCIS0 | CM_POS | CAP | SCS; \
		/* Wait until both timers capture the next clock tick */\
		while (!((TBCCTL6 & CCIFG) && (TACCTL2 & CCIFG))); \
		/* Store the capture timer values */\
		t_cap_h = TBCCR6; \
		t_cap_l = TACCR2; \
		/* Disable capture mode */\
		TBCCTL6 = 0; \
		TACCTL2 = 0; \
} while (0)

/** @} */

/* -------------------------------- SFD ----------------------------- */

/**
 * \defgroup glossy_sfd Management of SFD interrupts
 * @{
 */

/**
 * \brief Capture instants of SFD events on timer B1
 * \param edge Edge used for capture.
 *
 */
#define SFD_CAP_INIT(edge) do {\
	P4SEL |= BV(SFD);\
	TBCCTL1 = edge | CAP | SCS;\
} while (0)

/**
 * \brief Enable generation of interrupts due to SFD events
 */
#define ENABLE_SFD_INT()		do { TBCCTL1 |= CCIE; } while (0)

/**
 * \brief Disable generation of interrupts due to SFD events
 */
#define DISABLE_SFD_INT()		do { TBCCTL1 &= ~CCIE; } while (0)

/**
 * \brief Clear interrupt flag due to SFD events
 */
#define CLEAR_SFD_INT()			do { TBCCTL1 &= ~CCIFG; } while (0)

/**
 * \brief Check if generation of interrupts due to SFD events is enabled
 */
#define IS_ENABLED_SFD_INT()    !!(TBCCTL1 & CCIE)

/** @} */

#endif /* GLOSSY_H_ */

/** @} */
