 /*
 * Copyright (c)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Sudipta Saha <sudipta@iitbbs.ac.in>
 *
 */

/**
 * \file
 *         App core, source file.
 * \author
 *         Sudipta Saha <sudipta@iitbbs.ac.in>
 */

#include "app.h"
#include "glossy.h"

static uint8_t app_node_pos, app_initiator, app_rx_cnt, app_tx_cnt, app_tx_max, app_rx_cnt_main;


static neighbour_data_struct app_neighbours[MAX_NUM_NEIGHBOURS];
static uint8_t app_init_cnt= 0, app_init_node_id, app_sent_to[MAX_NEW_BIG];
static uint8_t app_num_neighbours = 0 , app_brodcast_cnt = 0 , app_num_slots;
static uint8_t app_recieved_node_id, app_recieved_brodcast_cnt;

static uint8_t sender_flag;


static uint8_t *app_packet;
static uint8_t app_data_len, app_packet_len, app_packet_len_tmp, app_header;
static uint8_t app_bytes_read, app_tx_relay_cnt_last, app_rx_relay_cnt_last, app_n_timeouts;
static volatile uint8_t app_state;
// static rtimer_clock_t app_t_rx_start, app_t_rx_stop, app_t_rx_stop_tmp, app_t_tx_start, app_t_tx_stop;
// static rtimer_clock_t app_t_rx_timeout, app_rx_timeout;
// static rtimer_clock_t app_T_irq;
static rtimer_clock_t app_t_stop, app_t_start;
static rtimer_callback_t app_cb;
static struct rtimer *app_rtimer;
static void *app_ptr;
//static unsigned short ie1, ie2, p1ie, p2ie, tbiv;
static unsigned short app_tbiv;

/*********** Analysis related variables ********/

static int diff, rssi, last_tx_relay_cnt, error, total, rssi_backup;
static uint8_t app_chain_cnt,app_chain_len,app_pos,app_num_nodes,app_num_tx,app_n_tx, app_num_slots;

static uint8_t power_array[MAX_NEW_BIG];
static uint8_t chain_cnt_array[MAX_NEW_BIG], node_id_array[MAX_NEW_BIG], index, recev_1, recev_2;

static rtimer_clock_t app_start_time, prev_backup;

static unsigned int app_current_itr=0;
static unsigned int data_counter = 0;
static uint8_t app_state_backup,app_rssi_field_backup,app_data_field_backup,app_len_field_backup;
static uint8_t app_chain_cnt_field_backup,app_relay_cnt_field_backup,app_chain_len_backup,app_data;
static float app_link_quality;

static uint8_t  high_T_irq, rx_timeout, bad_length, bad_header, bad_crc;

// static uint8_t chain_array[HOPS] = {1,9,17,29,36};

/*********** Analysis related variables ********/


inline void state_machine_app(unsigned short app_tbiv_){
	// read TBIV to clear IFG
	//app_tbiv = TBIV;

	app_tbiv = app_tbiv_;

	if (app_state == APP_STATE_WAITING && SFD_IS_1) {
		//			// packet reception has started
		app_begin_rx();
	} else {
		if (app_state == APP_STATE_RECEIVED && SFD_IS_1) {
			//				// packet transmission has started
			app_begin_tx();
		} else {
			if (app_state == APP_STATE_TRANSMITTING && !SFD_IS_1) {
				// packet transmission has finished
				app_end_tx();
			} else {
				if(app_state == APP_STATE_RECEIVING && !SFD_IS_1){
					app_end_rx();
				}else {
					if (app_state == APP_STATE_ABORTED) {
						// packet reception has been aborted
						app_state = APP_STATE_WAITING;
					} else {
						app_state = APP_STATE_WAITING;
	// 					if ((app_state == APP_STATE_WAITING) && (app_tbiv == TBIV_TBCCR4)) {

	// 						// initiator timeout
	// 						app_n_timeouts++;
	// 						if (app_rx_cnt == 0) {
	// 							// no packets received so far: send the packet again
	// 							app_tx_cnt = 0;
	// 							// set the packet length field to the appropriate value
	// 							APP_LEN_FIELD = app_packet_len_tmp;
	// 							//								// set the header field
	// 							APP_HEADER_FIELD = APP_HEADER | (app_header & ~APP_HEADER_MASK);
	// 							if (app_sync) {
	// 								APP_RELAY_CNT_FIELD = app_n_timeouts * APP_INITIATOR_TIMEOUT;
	// 							}
	// 							// copy the application data to the data field
	// 							memcpy(&APP_DATA_FIELD, app_data, app_data_len);
	// 							//								// set Glossy state
	// 							app_state = APP_STATE_RECEIVED;
	// 							state_radio = STATE_RADIO_TRANSMITTING;
	// 							//								// write the packet to the TXFIFO
	// 							radio_write_tx(app_packet, app_packet_len_tmp);
	// 							// start another transmission
	// 							radio_start_tx();
	// 							//								// schedule the timeout again
	// 							app_schedule_initiator_timeout();
	// 						} else {
	// 							//								// at least one packet has been received: just stop the timeout
	// 							app_stop_initiator_timeout();
	// 						}
	// 					} else {
	// 						if (app_tbiv == TBIV_TBCCR5) {
	// 							//								// rx timeout
	// 							if (app_state == APP_STATE_RECEIVING) {
	// 								// we are still trying to receive a packet: abort the reception
	// 								radio_abort_rx();
	// 								app_state = APP_STATE_WAITING;
	// 								state_radio = STATE_RADIO_WAITING;

	// #if APP_DEBUG
	// 								app_rx_timeout++;
	// #endif /* GLOSSY_DEBUG */
	// 							}
	// 							//								// stop the timeout
	// 							glossy_stop_rx_timeout();
	// 						} else {
	// 							if (app_state != APP_STATE_OFF) {
	// 								//									// something strange is going on: go back to the waiting state
	// 								radio_flush_rx();
	// 								app_state = APP_STATE_WAITING;
	// 								state_radio = STATE_RADIO_WAITING;
	// 							}
	// 						}
	// 					}
					}
				}
			}
		}
	}
}

/* --------------------------- Glossy process ----------------------- */
PROCESS(app_process, "App busy-waiting process");
PROCESS_THREAD(app_process, ev, data) {
	PROCESS_BEGIN();

	do {
		app_packet = (uint8_t *) malloc(128);
	} while (app_packet == NULL);

	while (1) {
		PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_POLL);
		// prevent the Contiki main cycle to enter the LPM mode or
		// any other process to run while Glossy is running
		while (APP_IS_ON() && RTIMER_CLOCK_LT(RTIMER_NOW(), app_t_stop));

#if COOJA
		// while (get_app_state() != APP_STATE_OFF);
#endif /* COOJA */
		// Glossy finished: execute the callback function
		dint();
		app_cb(app_rtimer, app_ptr);
		eint();
	}

	PROCESS_END();
}


/* --------------------------- Main interface ----------------------- */
void app_start(uint8_t app_initiator_, uint8_t app_num_slots_,

			 uint8_t app_header_, rtimer_clock_t app_t_stop_, rtimer_callback_t app_cb_,
			struct rtimer *app_rtimer_, void *app_ptr_) {
	// copy function arguments to the respective Glossy variables

	app_initiator = app_initiator_;

	app_num_slots = app_num_slots_;
	app_chain_len = app_num_slots + 4;
	app_chain_len_backup = app_chain_len;

	uint8_t i;
	app_header = app_header_;

	app_t_stop = app_t_stop_;
	app_cb = app_cb_;
	app_rtimer = app_rtimer_;
	app_ptr = app_ptr_;


	if(app_initiator)
		app_n_tx = 2;
	else
		app_n_tx = 1;

	app_num_tx = 1;
	
	// if(app_num_nodes % (app_chain_len-2) == 0) app_num_tx = app_num_nodes/(app_chain_len-2);
	// else app_num_tx = (app_num_nodes/(app_chain_len-2)) + 1;

	app_tx_max = app_n_tx * app_num_tx;
	app_rx_cnt_main = 0;
	app_data_len = 1;
	app_data = 1;

	for(i=0;i<app_chain_len;i++){
		power_array[i] = 0;
	}
	power_array[0]=1;
	power_array[app_chain_len-1]=1;

	//decide the sub-slot in which this node will trasmit
	app_pos= (node_id % app_num_slots);
	if(app_pos == 0)	app_pos = app_num_slots;

	power_array[app_pos] = 1;
	app_rx_cnt_main++;

	// disable all interrupts that may interfere with Glossy
	glossy_disable_other_interrupts();
	//	// initialize Glossy variables
	app_tx_cnt = 0;
	app_rx_cnt = 0;
	error = 0;
	total = 0;

	//for debugging
	bad_length = 0;
	bad_crc = 0;
	bad_header = 0;


	// app_rx_cnt_other = 0;
	sender_flag =0;
	app_current_itr = 0;
	app_chain_cnt = 0;
	app_init_node_id = 0;
	data_counter = 0;
	app_start_time = RTIMER_NOW();

	index = 0;
	recev_1 = 0;
	recev_2 = 0;

	APP_NODE_ID_FIELD = node_id;
	APP_CHAIN_CNT_FIELD = 0;

	// set Glossy packet length, with or without relay counter depending on the sync flag value
	// if (app_data_len) {
	app_packet_len_tmp = APP_PACKET_LENGTH;
	app_packet_len = app_packet_len_tmp;


		//		// set the packet length field to the appropriate value
	APP_LEN_FIELD = app_packet_len_tmp;
	// set the header field
	APP_HEADER_FIELD = APP_HEADER | (app_header & ~APP_HEADER_MASK);
	/*--------------------------------------The below values are printed correctly--------------------------------------------*/
	app_header_start=app_header;
	app_header_field_start=APP_HEADER_FIELD;

	if (app_initiator) {
		// initiator: copy the application data to the data field
		// memcpy(&APP_DATA_FIELD, app_data, app_data_len);
		// set Glossy state
		app_state = APP_STATE_RECEIVED;
		state_radio = STATE_RADIO_TRANSMITTING;

	} else {
		// receiver: set Glossy state
		app_state = APP_STATE_WAITING;
		state_radio = STATE_RADIO_WAITING;

	}

	fast_set_power(MAX_POWER_LEVEL);

#if !COOJA
	// resynchronize the DCO
	msp430_sync_dco();
#endif /* COOJA */

	// flush radio buffers
	radio_flush_rx();
	radio_flush_tx();

	if (app_initiator) {
		// write the packet to the TXFIFO

		make_packet();

		app_state = APP_STATE_WAITING;
		state_radio = STATE_RADIO_WAITING;

		// t_start = RTIMER_NOW();
		fast_set_power(MAX_POWER_LEVEL);
		radio_write_tx(app_packet, 5);
		fast_set_power(MIN_POWER_LEVEL);
		radio_write_tx(app_packet, 5);
		fast_set_power(MAX_POWER_LEVEL);
		radio_write_tx(app_packet, 7);
		// start the first transmission
		radio_start_tx();
		// schedule the initiator timeout
		//if ((!sync) || app_T_slot_h) {
		// app_n_timeouts = 0;
		// app_schedule_initiator_timeout();
		//}
	} else {
		// turn on the radio
		// t_start = RTIMER_NOW();
		radio_on();
	}
	// activate the Glossy busy waiting process
	process_poll(&app_process);
}

uint8_t app_stop(void) {
	// stop the initiator timeout, in case it is still active
	// app_stop_initiator_timeout();
	// turn off the radio
	radio_off();
	//
	//	// flush radio buffers
	radio_flush_rx();
	radio_flush_tx();
	//
	app_state = APP_STATE_OFF;
	state_radio = STATE_RADIO_WAITING;
	//	// re-enable non Glossy-related interrupts
	glossy_enable_other_interrupts();
	// return the number of times the packet has been received
	return app_rx_cnt;
}

void print_app_states(void){
	uint8_t i,j=0;
	// uint8_t prev_relay = 0;

#if APP_DEBUG
	printf("error:%2d, total:%2d || ", error, total);

	// printf("\n");
	// for(i=0; i<index; i++){
	// 	printf("cc:%2d, nID:%2d  |  ", chain_cnt_array[i], node_id_array[i]);
	// }
	// printf("\n");

	printf("bad header:%2d, bad length:%2d, bad crc:%2d |", bad_header, bad_length , bad_crc);
#endif

	printf("%2u, %2u, %2u,  %2u ", sender_flag, app_brodcast_cnt, app_num_slots, app_num_neighbours);

	for(i = 0; i < app_num_neighbours ; i++ ){
		// for(i=0; i< neighbours[i].neighbour_node_id -1 ; i++){
		// 	printf(", ");
		// }
		printf(",%2d,%2d,%2d,%d ",
		app_neighbours[i].neighbour_node_id, app_neighbours[i].recieved_from, app_init_cnt
		, app_neighbours[i].last_rssi );

		// printf("nodeID: %d  ", neighbours[i].neighbour_node_id );
	}
}


/* Main logic */

inline void make_packet(){

	// app_pos = APP_CHAIN_CNT_FIELD;

	if(app_initiator){
		app_init_cnt++;
		//late change so just sent node id in this feild instead of making lot if changes reg. INIT_FLAG
		APP_CHAIN_CNT_FIELD = node_id;
		APP_BRODCAT_CNT_FIELD = 0; 
		APP_NODE_ID_FIELD = INIT_FLAG;

	}else{
		if(app_chain_cnt == 0 || app_chain_cnt == app_chain_len - 1){
			APP_BRODCAT_CNT_FIELD = 0;
			APP_NODE_ID_FIELD = 0;
			fast_set_power(MAX_POWER_LEVEL);
		}
		else{
			srand(node_id );
			unsigned short r = rand();
			// app_pos = (r % app_num_slots);

			app_pos= (node_id % app_num_slots);
			if(app_pos == 0)	app_pos = app_num_slots;

			if(app_pos ==1 && app_chain_cnt == app_num_slots+1){
				//error handling, recievers were always missing the first slot packets packet
				APP_BRODCAT_CNT_FIELD = app_sent_to[app_init_node_id];
				APP_NODE_ID_FIELD = node_id;
				
				fast_set_power(MAX_POWER_LEVEL);
				return;			
			}
			if(app_pos ==2 && app_chain_cnt == app_num_slots+2){
				//error handling, otherwise slot 1 node will miss slot 2 node 
				APP_BRODCAT_CNT_FIELD = app_sent_to[app_init_node_id];
				APP_NODE_ID_FIELD = node_id;
				
				fast_set_power(MAX_POWER_LEVEL);
				return;			
			}

			if(app_chain_cnt == app_pos){
				
				app_brodcast_cnt++;
				app_sent_to[app_init_node_id]++;

				APP_BRODCAT_CNT_FIELD = app_sent_to[app_init_node_id];
				APP_NODE_ID_FIELD = node_id;
				
				fast_set_power(MAX_POWER_LEVEL);
			}else{
				APP_BRODCAT_CNT_FIELD = 0;
				APP_NODE_ID_FIELD = 0;
				fast_set_power(MIN_POWER_LEVEL);
			}
		}
	}
}

inline void rx_packet_data(){
	// Uncomment this when recievers of init packet reply only to initiator (for accurate prr)
#if PRR
	if(!app_initiator)
		return;
#endif

	if(app_recieved_node_id == INIT_FLAG)
		return;
	if(app_recieved_node_id == 0)
		return;

	app_pos= (app_recieved_node_id % app_num_slots);
	if(app_pos == 1 && recev_1)
		return;
	if(app_pos == 2 && recev_2)
		return;


	if(1){	//app_chain_cnt > 0 && app_chain_cnt < app_chain_len-1 

		uint8_t i, flag=0;
		for( i=0; i<app_num_neighbours; i++ ){
			//Check if neighbour already exists
			if(app_neighbours[i].neighbour_node_id == app_recieved_node_id ){
				flag=1;
				break;
			}
		}

		if(flag){
			//neighbour already exist, update the data
			app_neighbours[i].brodcast_from = app_recieved_brodcast_cnt;
			app_neighbours[i].recieved_from++;
			app_neighbours[i].last_rssi = app_rssi_field_backup;
// #if APP_DEB_PRINT
// printf("   !!  updated n:%2u, bf:%2u, rf:%2u  !!\n",
// app_neighbours[i].neighbour_node_id, app_neighbours[i].brodcast_from, app_neighbours[i].recieved_from);
// #endif
		}else{
			//new neighbour, create and add new data
			if(app_num_neighbours < MAX_NUM_NEIGHBOURS ){

				app_neighbours[app_num_neighbours].neighbour_node_id = app_recieved_node_id;
				app_neighbours[app_num_neighbours].brodcast_from = app_recieved_brodcast_cnt;
				app_neighbours[app_num_neighbours].recieved_from = 1;
				app_neighbours[i].last_rssi = app_rssi_field_backup;

				app_num_neighbours++;
// #if APP_DEB_PRINT
// printf("   !!  added n:%2u, bf:%2u, rf:%2u  !!\n",
// app_neighbours[i+1].neighbour_node_id, app_neighbours[i+1].brodcast_from, app_neighbours[i+1].recieved_from);
// #endif
			}
		}

		if(app_pos == 1)
			recev_1 = 1;
		if(app_pos == 2)
			recev_2 = 1;
	}	

	app_rx_cnt++;

}

/* Main logic */
uint8_t get_sender_flag(void) {
	return sender_flag;
}
uint8_t get_app_brodcast_cnt(void) {
	return app_brodcast_cnt;
}
uint8_t get_app_num_neighbours(void) {
	return app_num_neighbours;
}
uint8_t get_app_init_cnt(void) {
	return app_init_cnt;
}
neighbour_data_struct * get_app_neighbours(void){
	return app_neighbours;
}

uint8_t get_app_rx_cnt(void) {
	return app_rx_cnt;
}
uint8_t get_app_tx_cnt(void) {
	return app_tx_cnt;
}
uint8_t get_app_state(void) {
	return app_state;
}
uint8_t get_packet_len(void) {
	return app_packet_len_tmp;
}
uint8_t get_app_rx_cnt_main(void) {
	return app_rx_cnt_main;
}
///* ----------------------- Interrupt functions ---------------------- */

inline void app_end_rx_or_tx(){
	if(app_state == APP_STATE_TRANSMITTING) app_end_tx();
	else app_end_rx();
}

inline void app_begin_rx(void) {
	// app_t_rx_start = TBCCR1;
	app_state = APP_STATE_RECEIVING;

	total++;

	// //last reception should be immediately followed by transmission
	// if(app_chain_cnt == app_chain_len-1) state_radio = STATE_RADIO_RECEIVING_FOR_SYNCH;
	// //any other reception is followed by a reception
	// else state_radio = STATE_RADIO_RECEIVING_NORMAL;

	state_radio = STATE_RADIO_RECEIVING_NORMAL;

	while (!FIFO_IS_1) {
	};

	// read the first byte (i.e., the len field) from the RXFIFO
	FASTSPI_READ_FIFO_BYTE(APP_LEN_FIELD);

	// keep receiving only if it has the right length
// 	if ((APP_LEN_FIELD < APP_FOOTER_LEN) || (APP_LEN_FIELD > 127)) {
// 		// packet with a wrong length: abort packet reception
// 		radio_abort_rx();
// 		app_state = APP_STATE_WAITING;
// 		state_radio = STATE_RADIO_WAITING;
// 		error++;

// 		// app_state_storage[app_current_itr++] = 4;

// #if APP_DEBUG
// 		bad_length++;
// #endif /* GLOSSY_DEBUG */
// 		return;
// 	}

	app_bytes_read = 1;

	app_packet_len_tmp = APP_LEN_FIELD;
	app_packet_len = app_packet_len_tmp;

#if COOJA

	FASTSPI_READ_FIFO_BYTE(APP_HEADER_FIELD);
	// keep receiving only if it has the right header
	/*--------------------------------------The below values are printed 0?????--------------------------------------------*/
	app_header_field_curr=APP_HEADER_FIELD;
	app_header_curr = APP_HEADER_FIELD & ~APP_HEADER_MASK;
	app_header_org=app_header;
	if (((APP_HEADER_FIELD & APP_HEADER_MASK) != APP_HEADER) ||((APP_HEADER_FIELD & ~APP_HEADER_MASK) != app_header))
		// packet with a wrong header: abort packet reception
		//if ((APP_HEADER_FIELD & APP_HEADER_MASK) != APP_HEADER )
	{
		bad_header++;
		radio_abort_rx();
		app_state = APP_STATE_WAITING;
		state_radio = STATE_RADIO_WAITING;
		error++;

		// app_state_storage[app_current_itr++] = 5;
		return;
	}
	app_bytes_read = 2;

	FASTSPI_READ_FIFO_BYTE(APP_CHAIN_CNT_FIELD);

	app_bytes_read = 3;

	FASTSPI_READ_FIFO_BYTE(APP_NODE_ID_FIELD);
	
	app_recieved_node_id = APP_NODE_ID_FIELD;

	app_bytes_read = 4;

	if(APP_CHAIN_CNT_FIELD == app_chain_len-1 || app_recieved_node_id == INIT_FLAG)
		//recieved a init packet
		state_radio = STATE_RADIO_RECEIVING_FOR_SYNCH;	


#elif !COOJA

	while (!FIFO_IS_1) {
	};
	// read the second byte (i.e., the header field) from the RXFIFO
	FASTSPI_READ_FIFO_BYTE(APP_HEADER_FIELD);
	// keep receiving only if it has the right header
	/*--------------------------------------The below values are printed 0?????--------------------------------------------*/
	app_header_field_curr=APP_HEADER_FIELD;
	app_header_curr = APP_HEADER_FIELD & ~APP_HEADER_MASK;
	app_header_org=app_header;
	if (((APP_HEADER_FIELD & APP_HEADER_MASK) != APP_HEADER) ||((APP_HEADER_FIELD & ~APP_HEADER_MASK) != app_header))
		// packet with a wrong header: abort packet reception
		//if ((APP_HEADER_FIELD & APP_HEADER_MASK) != APP_HEADER )
	{
		//bad_hdr_cnt++;
		radio_abort_rx();
		app_state = APP_STATE_WAITING;
		state_radio = STATE_RADIO_WAITING;
		error++;

		// app_state_storage[app_current_itr++] = 5;

#if APP_DEBUG
		app_bad_header++;
#endif /* GLOSSY_DEBUG */
		return;
	}
	app_bytes_read = 2;

	while(!FIFO_IS_1);
	FASTSPI_READ_FIFO_BYTE(APP_CHAIN_CNT_FIELD);

	app_bytes_read = 3;

	while(!FIFO_IS_1);
	FASTSPI_READ_FIFO_BYTE(APP_NODE_ID_FIELD);
	app_recieved_node_id = APP_NODE_ID_FIELD;;

	app_bytes_read = 4;

	if(APP_CHAIN_CNT_FIELD == app_chain_len-1 || app_recieved_node_id == INIT_FLAG)
		//recieved a init packet
		state_radio = STATE_RADIO_RECEIVING_FOR_SYNCH;	

#endif /* COOJA */

	// glossy_schedule_rx_timeout(app_t_rx_timeout);
}

inline void app_end_rx(void) {
	// rtimer_clock_t app_t_rx_stop_tmp = TBCCR1;
	// read the remaining bytes from the RXFIFO
	FASTSPI_READ_FIFO_NO_WAIT(&app_packet[app_bytes_read], app_packet_len_tmp - app_bytes_read + 1);
	app_bytes_read = app_packet_len_tmp + 1;
#if COOJA
	if ((APP_CRC_FIELD & APP_FOOTER1_CRC_OK) && ((APP_HEADER_FIELD & APP_HEADER_MASK) == APP_HEADER)) {
#else
	if (APP_CRC_FIELD & APP_FOOTER1_CRC_OK) {
#endif /* COOJA */

		app_state_backup = app_state;
		app_chain_cnt_field_backup = APP_CHAIN_CNT_FIELD;
		app_chain_cnt = app_chain_cnt_field_backup;
		app_recieved_brodcast_cnt = APP_BRODCAT_CNT_FIELD;
		app_rssi_field_backup = APP_RSSI_FIELD;
		app_recieved_node_id = APP_NODE_ID_FIELD;
		app_len_field_backup = APP_LEN_FIELD;

#if APP_DEB_PRINT
// if(app_chain_cnt == 1){
// 	printf("  !!  nodeID:%2u , bc:%2u, cc: %2u  !!\n", app_recieved_node_id, app_brodcast_cnt, app_chain_cnt);
// }
// if(bad_length){
// 	printf("  !! bad len at nodeID:%2u , bc:%2u, cc: %2u  !!\n", app_recieved_node_id, app_brodcast_cnt, app_chain_cnt);
// 	bad_length = 0;
// }
#endif

		if(app_recieved_node_id == INIT_FLAG){
			// printf("brod: %2d, cc: %2d\n", APP_BRODCAT_CNT_FIELD, APP_CHAIN_CNT_FIELD);
			app_init_node_id = APP_CHAIN_CNT_FIELD; //node id sent in chain cnt feild
			sender_flag =1;
		}

		if(state_radio == STATE_RADIO_RECEIVING_FOR_SYNCH){

			if(app_recieved_node_id != INIT_FLAG){
				//recieved a body packet, turn off
				radio_off();
				app_state = APP_STATE_OFF;
				state_radio = STATE_RADIO_WAITING;
				return;
			}

			if (app_tx_cnt == app_tx_max) {
				// no more Tx to perform: stop Glossy
				radio_off();
				app_state = APP_STATE_OFF;
				state_radio = STATE_RADIO_WAITING;
			}
			
			else{

				if (app_state == APP_STATE_RECOVERY) {
					// transmit the next packet in the chain
					// if you recieved the last packet, rare case, see later
					app_chain_cnt = APP_CHAIN_CNT_FIELD;
					app_chain_cnt++;
					APP_CHAIN_CNT_FIELD = app_chain_cnt;

				}else {
					// write Glossy packet to the TXFIFO
					app_chain_cnt = 0;
					APP_CHAIN_CNT_FIELD = app_chain_cnt;
				}

				make_packet();

				radio_write_tx(app_packet, app_packet_len_tmp);
				app_state = APP_STATE_RECEIVED;
				state_radio = STATE_RADIO_TRANSMITTING;
			}

		}else{

			app_state = APP_STATE_WAITING;
			state_radio = STATE_RADIO_WAITING;

			// radio_flush_tx();		
		}

		/*  ************************ SETTING VARIABLES  ************************ */
		rx_packet_data();

		/*  ************************ SETTING VARIABLES  ************************ */


	} else {

		// app_state_storage[app_current_itr++] = 6;

#if APP_DEBUG
		bad_crc++;
#endif /* GLOSSY_DEBUG */

		// packet corrupted, abort the transmission before it actually starts
		radio_abort_tx();
		app_state = APP_STATE_WAITING;
		state_radio = STATE_RADIO_WAITING;
		error++;
	}
}
//
inline void app_begin_tx(void) {
	// app_t_tx_start = TBCCR1;
	app_state = APP_STATE_TRANSMITTING;

	// last transmission needs to be followed by reception 
	if(app_chain_cnt == app_chain_len-1) state_radio = STATE_RADIO_TRANSMITTING;
	//every other transmission needs to be immediately followed by another transmission
	else state_radio = STATE_RADIO_RECEIVING_FOR_SYNCH;

}

inline void app_end_tx(void) {

	if(state_radio == STATE_RADIO_RECEIVING_FOR_SYNCH){
		app_chain_cnt++;
		APP_CHAIN_CNT_FIELD = app_chain_cnt;

		make_packet();

		// write Glossy packet to the TXFIFO
		radio_write_tx(app_packet, app_packet_len_tmp);
		app_state = APP_STATE_RECEIVED;
		state_radio = STATE_RADIO_TRANSMITTING;	

	}else{	
		app_chain_cnt = 0;
		ENERGEST_OFF(ENERGEST_TYPE_TRANSMIT);
		ENERGEST_ON(ENERGEST_TYPE_LISTEN);	
		// stop Glossy if tx_cnt reached tx_max (and tx_max > 1 at the initiator)
		app_tx_cnt++;
		if ( !app_initiator) { //(app_tx_cnt == app_tx_max) &&
			radio_off();
			app_state = APP_STATE_OFF;
			state_radio = STATE_RADIO_WAITING;
		}else{
			app_state = APP_STATE_WAITING;
			state_radio = STATE_RADIO_WAITING;
		}
		radio_flush_tx();
	}	
	
}

// inline void app_schedule_initiator_timeout() {
// #if !COOJA
// 	TBCCR4 = app_t_start + (app_n_timeouts + 1) * APP_INITIATOR_TIMEOUT *
// 			((rtimer_clock_t)app_packet_len * 35 + 400) * 4;

// 	TBCCTL4 = CCIE;
// #endif
// }

// inline void app_stop_initiator_timeout(void) {
// 	TBCCTL4 = 0;
// }

// void print_power_array(void){
// 	uint8_t index;

// 	for(index=0;index<app_num_tx*app_chain_len;index++){
// 		if(index % app_chain_len == 0) printf(" ");
// 		printf("%d ",power_array[index]);
// 	}
// }
